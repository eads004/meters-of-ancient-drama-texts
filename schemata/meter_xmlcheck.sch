<?xml version="1.0" encoding="UTF-8"?>
<schema xmlns="http://purl.oclc.org/dsdl/schematron" xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns:sqf="http://www.schematron-quickfix.com/validator/process" queryBinding="xslt2">
    <ns prefix="tei" uri="http://www.tei-c.org/ns/1.0"/>
    <pattern>
        <rule context="tei:div1">
            <assert test="@type">Every div1 must have a @type.</assert>
            <assert test="
                    (@type = 'Prologos') or (@type = 'Parodos')
                    or (@type = 'Exodos') or (@type = 'Monody')
                    or (@type = '1stEpisode') or (@type = '2ndEpisode') or (@type = '3rdEpisode')
                    or (@type = '4thEpisode') or (@type = '5thEpisode')
                    or (@type = '1stStasimon') or (@type = '2ndStasimon') or (@type = '3rdStasimon')
                    or (@type = '4thStasimon') or (@type = '5thStasimon')">Every div1
                @type must match an expected div1 name.</assert>
        </rule>
        <rule context="tei:milestone">
            <assert test="@unit">A milestone must have a @unit attribute.</assert>
            <assert test="
                    (@unit = 'word') or (@unit = 'speaker') or (@unit = 'period') or (@unit = 'synapheia')
                    or (@unit = 'comment')">A milestone's @unit
                attribute must be word, speaker, period, synapheia, or comment.</assert>
        </rule>
        <rule context="tei:l">
            <assert
                test="ancestor-or-self::*[@type = 'lyric' or @type = 'nonlyric' or @type = 'unknown']"
                >Every line must be classifiable as lyric or nonlyric or unknown, either directly or
                through an ancestor.</assert>
        </rule>
        <rule
            context="tei:seg">
            <assert test="(@type = 'syll') or (@type = 'line_text') or (@type = 'metron')">Every seg must be of type syll or line_text or metron.</assert>
        </rule>
    </pattern>
    <pattern>
        <rule context="tei:div2[@ana]">
            <assert test="(@ana = 'strophe') or (@ana = 'antistrophe') or (@ana = 'epode') or (@ana = 'mesode') or (@ana = 'kommos')">A div2
                @ana must be strophe, antistrophe, mesode, kommos, or epode.</assert>
        </rule>
    </pattern>
    <pattern>
        <rule context="tei:div2[@met]">
            <assert test="(@met = 'ia3') or (@met = 'anapests') or (@met = 'tr4^')">A div2 @met must
                be ia3, anapests, or tr4^.</assert>
        </rule>
    </pattern>
    <pattern>
        <rule context="tei:div2[@type]">
            <assert test="@type = 'lyric' or @type = 'nonlyric' or @type = 'unknown'">Every @type
                attribute on a div2 must be classifiable as lyric, nonlyric, or unknown.</assert>
        </rule>
    </pattern>
    <pattern>
        <rule context="tei:milestone[@unit = 'speaker']">
            <assert test="@n">A speaker milestone must have an @n attribute.</assert>
        </rule>
    </pattern>
    <pattern>
        <rule context="tei:seg[@type = 'syll']">
            <assert test="(@ana = 'short') or (@ana = 'long') or (@ana = 'shortn') or (@ana = 'x')">Every seg of type syll must have an
                @ana attribute with the value 'short' or 'long' or 'shortn' or 'x'.</assert>
        </rule>
    </pattern>
    <pattern>
        <rule context="tei:seg[@type = 'metron']">
            <assert test="not(./tei:seg[@type != 'syll'])">Every seg of @type 'metron' must have seg children that are only of @type 'syll'.</assert>
        </rule>
    </pattern>
</schema>
