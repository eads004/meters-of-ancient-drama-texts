#===============================================================#
#  	This shell script puts the XML of the			#
#  	Ancient Meter Project into compliance to the		#
#  	schemata drafted in the ../texts/schemata directory	#
#===============================================================#

#===============================================================#
#	Prerequisites: Hand-checked, Syllabified XML		#
#===============================================================#

#===============================================================#
#  	The process currently utilizes a dummy directory to	#
#  	ensure we don't fumble all of the hard work done	#
#  	thus far, either by hand or by code.			#
#								#
#  	Once a sure-fire method has been developed, we can	#
#  	simply output the schematized XML directly into the	#
#  	working directories of the website.			#
#===============================================================#

repodir=$(dirname $0)/..

#===============================================================#
#		Clean out the destination directory		#
#===============================================================#

rm -rf $repodir/texts/schematized_XML
mkdir $repodir/texts/schematized_XML

#===============================================================#
#		Move <seg line_text>ABC</seg>			#
#		to <l n="#" line_text="ABC">			#
#	N.B. this process may become obsolete after publishing	#
#			-Bacchae				#
#===============================================================#

#  If we figure out how to go from base XML to something resembling a well-formed XML sans hand-edits
#  we could run: for i in $repodir/texts/RAW_XML/Euripides/*_Greek.xml or something
for i in $repodir/texts/*.xml;
do
	java -jar $repodir/saxon/saxon-he-11.3.jar -xsl:$repodir/texts/move_line_text_elements.xsl -s:$i -o:$repodir/texts/schematized_XML/`basename "$i"`;
done

# for i in $repodir/texts/RAW_XML/Euripides/*_Greek.xml
# do
#	scansion script
#	put template data in based on where l=n"#"
# done

#===============================================================#
#		DEVELOPING THIS SCRIPT FURTHER:			#	
#	1) Insert div1 and div2 according to a template		#
#	2) Write some python utilizing xsl to insert synapheia,	#
#		and periods according to Tim's templates.	#
#	3) Perhaps include metra for the lyric sections.	#
#	4) Automate populating the XML with proper name tags.	#
#	5) Would we ever like to mark up resolution?		#
#===============================================================#

