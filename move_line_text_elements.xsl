<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" 
    xmlns:tei="http://www.tei-c.org/ns/1.0"
    xmlns="http://www.tei-c.org/ns/1.0"
    exclude-result-prefixes="xs tei" version="3.0">

    <xsl:mode on-no-match="shallow-copy"/>

    <xsl:template match="tei:l">
            <xsl:copy>
                <xsl:apply-templates select="./@*"/>
                <xsl:attribute name="line_text">
                    <xsl:apply-templates select="normalize-space(string-join(.//tei:seg[@type = 'line_text']/text(), ' '))"/>
                </xsl:attribute>
                <xsl:apply-templates select="*|text()[position() != 1]"/>
            </xsl:copy>
    </xsl:template>

    <xsl:template match="tei:seg[@type = 'line_text']"/>

</xsl:stylesheet>
